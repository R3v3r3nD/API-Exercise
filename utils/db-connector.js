var mysql = require('mysql');

var connection = mysql.createConnection({
    host  : process.env.MYSQL_IP,
    port  : '3306',
    user  : 'root',
    password  : process.env.MYSQL_PASS,
    database  : 'db1'
  });

connection.connect(function(err) {
	if (err) {
	  console.error('error connecting: ' + err.stack);
	} else { 
	console.log('connected as id ' + connection.threadId);
	}
});

module.exports = connection;

