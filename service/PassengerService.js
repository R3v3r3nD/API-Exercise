'use strict';

var db = require('../utils/db-connector.js');

/**
 * Add a new passenger
 * 
 **/
exports.addPassenger = function(body) {
  return new Promise(function(resolve, reject) {
    var sqlQuery = "INSERT INTO titanic (`Survived`, `Pclass`, `Siblings/Spouses Aboard`, `Parents/Children Aboard`, `Sex`, `Age`, `Name`, `Fare`) VALUES (?, ?, ?, ?, ?, ?, ?, ?)";
    var bodyArr = [];
    for (var field in body) {
      if (body.hasOwnProperty(field)) {
        bodyArr.push(body[field]);
      }
    }
    db.query(sqlQuery, bodyArr, function(error, results, fields){
      if (error){
        reject(JSON.stringify({"status": 500, "error": error, "response": null}));
      } else {
        resolve(JSON.stringify({"status": 200, "error": null, "response": results}));
      }
    });
  });
}


/**
 * Delete passenger
 *
 **/
exports.deletePassenger = function(id) {
  return new Promise(function(resolve, reject) {
    db.query('DELETE FROM titanic WHERE id = ?', [id], function(error, results, fields){
      if (error){
        reject(JSON.stringify({"status": 500, "error": error, "response": null}));
      } else {
        resolve(JSON.stringify({"status": 200, "error": null, "response": results}));
      }
    });
  });
}


/**
 * Retrieves all passengers
 * 
 **/
exports.getPassengers = function() {
  return new Promise(function(resolve, reject) {
    db.query('SELECT * FROM titanic', function(error, results, fields){
      if (error){
        reject(JSON.stringify({"status": 500, "error": error, "response": null}));
      } else {
        resolve(JSON.stringify({"status": 204, "error": null, "response": results}));
      }
    });
  });
}


/**
 * Update existing passenger
 *
 **/
exports.updatePassenger = function(id,body) {
  return new Promise(function(resolve, reject) {
    var sqlQuery = "UPDATE titanic SET `Survived` = ?, `Pclass` = ?, `Siblings/Spouses Aboard` = ?, `Parents/Children Aboard` = ?, `Sex` = ?, `Age` = ?, `Name` = ?, `Fare` = ? WHERE id = ?";
    var bodyArr = [];
    for (var field in body) {
      if (body.hasOwnProperty(field)) {
        bodyArr.push(body[field]);
      }
    }
    bodyArr.push(id);
    db.query(sqlQuery, bodyArr, function(error, results, fields){
      if (error){
        reject(JSON.stringify({"status": 500, "error": error, "response": null}));
      } else {
        resolve(JSON.stringify({"status": 200, "error": null, "response": results}));
      }
    });
  });
}

