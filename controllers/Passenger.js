'use strict';

var utils = require('../utils/writer.js');
var Passenger = require('../service/PassengerService');

module.exports.addPassenger = function addPassenger (req, res, next) {
  var body = req.swagger.params['body'].value;
  Passenger.addPassenger(body)
    .then(function (response) {
      utils.writeJson(res, response);
    })
    .catch(function (response) {
      utils.writeJson(res, response);
    });
};

module.exports.deletePassenger = function deletePassenger (req, res, next) {
  var id = req.swagger.params['id'].value;
  Passenger.deletePassenger(id)
    .then(function (response) {
      utils.writeJson(res, response);
    })
    .catch(function (response) {
      utils.writeJson(res, response);
    });
};

module.exports.getPassengers = function getPassengers (req, res, next) {
  Passenger.getPassengers()
    .then(function (response) {
      utils.writeJson(res, response);
    })
    .catch(function (response) {
      utils.writeJson(res, response);
    });
};

module.exports.updatePassenger = function updatePassenger (req, res, next) {
  var id = req.swagger.params['id'].value;
  var body = req.swagger.params['body'].value;
  Passenger.updatePassenger(id,body)
    .then(function (response) {
      utils.writeJson(res, response);
    })
    .catch(function (response) {
      utils.writeJson(res, response);
    });
};
